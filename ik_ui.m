function ik_ui
    robert = build_robot([0 1], [0 1]);
    setup_ui(robert);
    update_angles(0, 1, 0, 1, robert, 0, 0, 0);
end

function setup_ui(robot)
    pos = get(gca,'position')
    position = [pos(1)+0.6 pos(2)+0.7 pos(3)*0.3 0.05];
	angle_1_slider = uicontrol(...
        'style', 'slider',...
		'units', 'normalized',...
        'position', position,...
        'min', -pi/2,...
        'max', pi/2);
    position = [pos(1)+0.6 pos(2)+0.6 pos(3)*0.3 0.05];
	angle_2_slider = uicontrol(...
        'style', 'slider',...
		'units', 'normalized',...
        'position', position,...
        'min', -pi/2,...
        'max', pi/2);
    position = [pos(1)+0.5 pos(2)+0.7 pos(3)*0.1 0.05];
    angle_1_text = uicontrol(...
        'style', 'text',...
		'units', 'normalized',...
        'position', position,...
        'string', 'Phi 1: 0°');
    position = [pos(1)+0.5 pos(2)+0.6 pos(3)*0.1 0.05];
    angle_2_text = uicontrol(...
        'style', 'text',...
		'units', 'normalized',...
        'position', position,...
        'string', 'Phi 2: 0°');
    position = [pos(1)+0.7 pos(2)+0.5 pos(3)*0.2 0.05];
    coordinate_text = uicontrol(...
        'style', 'text',...
		'units', 'normalized',...
        'position', position,...
        'string', 'x:0 y:2');
    position = [pos(1)+0.6 pos(2)+0.5 pos(3)*0.1 0.05];
    ik_checkbox = uicontrol(...
        'style', 'checkbox',...
		'units', 'normalized',...
        'position', position,...
        'string', 'IK',...
        'min', 0,...
        'max', 1);
    callback = @(c, event) update_angles(...
        angle_1_slider.Value, 1,...
        angle_2_slider.Value, 1,...
        robot,...
        ik_checkbox.Value,...
        angle_1_text,...
        angle_2_text,...
        coordinate_text);
    angle_1_slider.Callback = callback;
    angle_2_slider.Callback = callback;
    set(figure(1), 'WindowButtonDownFcn', @(~,~)update_point(robot, coordinate_text));
end

function update_angles(phi_1, l_1, phi_2, l_2, robot, do_ik, text_1, text_2, coordinate_text)
    phi_1 = -phi_1;
    phi_2 = -phi_2;
    [v_1, v_2] = get_vectors_by_angles(phi_1, l_1, phi_2, l_2);
    v_12 = v_1 + v_2;
    draw_base();
    hold on;
    draw_arm_by_angles(phi_1, l_1, phi_2, l_2, 'b');
    if(do_ik == 1)
        draw_robot(robot, v_12);
    end
    hold off;
    phi_1 = -phi_1/(2*pi)*360;
    phi_2 = -phi_2/(2*pi)*360;
    
    if(text_1 ~= 0 && text_2 ~= 0 && coordinate_text ~= 0)
        text_1.String = append('Phi 1: ', string(round(phi_1)), '°');
        text_2.String = append('Phi 2: ', string(round(phi_2)), '°');
        coordinate_text.String = append('x:', string(round(v_12(1), 2)), ' y:', string(round(v_12(2), 2)));
    end
    
    if(~outside_working_space(v_1, v_1 + v_2))
        two_angles(phi_1, phi_2);
    end
end

function update_point(robot, coordinate_text)
    pointer = get(gca, 'CurrentPoint');
    point = [pointer(1,1) pointer(1,2)];
    draw_base();
    hold on;
    joint_configuration = draw_robot(robot, point);
    hold off;
    phi_1 = joint_configuration(1);
    phi_2 = joint_configuration(2);
    % TODO: get from robot
    [v_1, ~] = get_vectors_by_angles(-phi_1, 1, -phi_2, 1);
    phi_1 = phi_1/(2*pi)*360;
    phi_2 = phi_2/(2*pi)*360;
    
    if(coordinate_text ~= 0)
        coordinate_text.String = append('x:', string(round(point(1), 2)), ' y:', string(round(point(2), 2)));
    end
        
    if(~outside_working_space(v_1, point))
        two_angles(phi_1, phi_2);
    end
end

function joint_configuration = draw_robot(robot, point)
    ik = inverseKinematics('RigidBodyTree', robot);
    endEffector = 'tool';
    point = trvec2tform([point 0]);
    weights = [0.002, 0.002, 0.002, 1, 1, 0];
    guess = homeConfiguration(robot);
    joint_configuration = ik(endEffector, point, weights, guess);
%     show(robot,joint_configuration);
    % TODO: get lengths robot
    phi_1 = -joint_configuration(1);
    phi_2 = -joint_configuration(2);
    draw_arm_by_angles(phi_1, 1, phi_2, 1, 'g');
end

function draw_arm_by_angles(phi_1, l_1, phi_2, l_2, default_color)
    [v_1, v_2] = get_vectors_by_angles(phi_1, l_1, phi_2, l_2);
    draw_arm_by_vectors(v_1, v_2, default_color);
end

function draw_arm_by_vectors(v_1, v_2, default_color)
    v_12 = v_1 + v_2;
    color = default_color;
    if(outside_working_space(v_1, v_12))
        color = 'r';
    end
    plot([0 v_1(1) v_12(1)], [0 v_1(2) v_12(2)], color, "linewidth", 3);
end

function draw_base()
    hold off;
    subplot(1, 3, 1:2);
    plot([-2.2 2.2], [0 0], 'g');
    axis('equal');
    axis('manual');
    axis([-2.2 2.2 -0.2 2.2]);
    grid on;
end

function [v_1, v_2] = get_vectors_by_angles(phi_1, l_1, phi_2, l_2)
    R_1 = [cos(phi_1), -sin(phi_1); sin(phi_1), cos(phi_1)];
    v_1 = [0 l_1];
    v_1 = v_1 * R_1;
    R_2 = [cos(phi_2), -sin(phi_2); sin(phi_2), cos(phi_2)];
    v_2 = [0 l_2];
    v_2 = v_2 * R_1 * R_2;
end

function robot = build_robot(v_1, v_2)
    robot = rigidBodyTree('DataFormat','column','MaxNumBodies',2);
    body = rigidBody('link1');
    joint = rigidBodyJoint('joint1', 'revolute');
    setFixedTransform(joint,trvec2tform([0 0 0]));
    joint.JointAxis = [0 0 1];
    body.Joint = joint;
    addBody(robot, body, 'base');
    
    body = rigidBody('link2');
    joint = rigidBodyJoint('joint2', 'revolute');
    setFixedTransform(joint,trvec2tform([v_1 0]));
    joint.JointAxis = [0 0 1];
    body.Joint = joint;
    addBody(robot, body, 'link1');
    
    body = rigidBody('tool');
    joint = rigidBodyJoint('fix1','fixed');
    setFixedTransform(joint, trvec2tform([v_2 0]));
    body.Joint = joint;
    addBody(robot, body, 'link2');
end

function is_outside = outside_working_space(point1, point2)
    is_outside = point1(2) < 0 || point2(2) < 0;
end